# Build locally from a working directory

If you have a local repository, which you use for development and want to create
a build from this, you can do the following:

```
$ cd ../scratch-gui
$ NODE_ENV=production npm run build
```

The result is in the `build` directory.

# Build with GitLab CI
If you want to test your GitLab CI file locally you can install a local GitLab
runner and execute the following command:

```
$ sudo gitlab-runner exec docker --docker-volumes `pwd`/../../gitlab_builds:/builds pages
```

You will find the results in the `../../gitlab_builds` folder.

For example you can launch a statict HTTP server from the `public` folder of the
task. Example:

```
$ http-server ../../gitlab_builds/0/project-0/public
```
