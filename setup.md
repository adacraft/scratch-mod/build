# How to setup a adacraft developpement environment with docker

The instructions below use Docker Desktop and Visual Studio Code as a developpement environment. Of course, it's an example, and you can use any other tools. In this case only skip specific instructions about Docker Desktop and VS Code.
## Creating a Docker Container for adacraft

First, install [Docker Desktop](https://www.docker.com/products/docker-desktop) and [Visual Studio Code](https://code.visualstudio.com/).

Run the command `docker pull node:16-buster` to pull the image needed for the Docker container.

Then, inside of Docker Desktop, click on the **Images** tab on the sidebar. Click the "RUN ▶ " button on the **node** line, and click "Run".<br>
💡 You can set "adacraft" as your container name in the optional settings dropdown.

## How to navigate inside of your container using VS Code

Now that your container is running (if it's not, click the play button inside the **Container / Apps** section on the left), we can connect to your container with VS Code.

Open VS Code and input `CTRL + SHIFT + P`, that's the command palette. Inside of it type "running container" and select the "Remote-Containers: Attach to Running Container..." option using ENTER.

Select your container and VS Code should open it inside of your window.

From there, you can open folders and files using the "File" tab at the top of your window.

## Setting up your SSH key inside of the Docker Container

First, open up this link : [https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair](https://docs.gitlab.com/ee/ssh/#generate-an-ssh-key-pair)
This is the guide you'll need to follow for creating a SSH key so you can pull code in SSH mode as well as push to the adacraft repositories.

Once it is created you can add it here: [https://gitlab.com/-/profile/keys](https://gitlab.com/-/profile/keys)

## Getting the repositories from GitLab

The instructions below are inspired by the GitLab CI script (see file 
[.gitlab-ci.yml](.gitlab-ci.yml))


Inside VS Code, open the `/root/` folder and create a new folder named `adacraft` inside of it. This is where we'll get all of our repositories.

First, run this command to install OpenJDK - needed for later.

```
apt-get update && apt-get install -y openjdk-11-jdk
``` 

Then, inside the `/adacraft` directory, get all the adacraft repositories:

```
git clone git@gitlab.com:adacraft/scratch-mod/scratch-blocks.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-gui.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-l10n.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-paint.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-render.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-svg-renderer.git
git clone git@gitlab.com:adacraft/scratch-mod/scratch-vm.git
```

And then you'll need to install all dependencies for each repository:

```
( cd scratch-blocks && npm install )
( cd scratch-l10n && npm install && npm run build )
( cd scratch-paint && npm install )
# Order is important here. There are some errors if we build "render" before
# "svg-renderer".
( cd scratch-svg-renderer && npm install )
( cd scratch-render && npm install )
( cd scratch-vm && npm install )
( cd scratch-gui && npm install )
```

## Running a dev version of adacraft

Open up the `scratch-gui` repo, and then launch the following command in the VS Code terminal: `npm run start`.

Then click the "Open in Browser" button in the VS Code pop-up saying you have an application running on port XXXX.

## Building adacraft

Open up the `scratch-gui` repo, and then launch the following command in the VS Code terminal: `npm run build`.

To build for production, launch this command: `NODE_ENV=production npm run build`

The result of the build is in the `build` directory.
